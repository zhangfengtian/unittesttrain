/**
 * File name:UserService.java
 * Created by:lisanlai
 * Created date:下午1:50:32
 */
package com.cmcc.zysoft.train.service;

import com.cmcc.zysoft.train.model.UserVO;

/**
 * @author lisanlai
 * @mail sanlai_lee@qq.com
 * @date 2013-6-5 下午1:50:32
 */
public interface UserService {
	
	/**
	 * 用户登录逻辑
	 * @param name 用户名 
	 * @param password 密码
	 * @return 登录成功返回用户对象，登录失败返回null
	 */
	public UserVO login(String name,String password);
}
