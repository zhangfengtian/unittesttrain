/**
 * File name:UserController.java
 * Created by:lisanlai
 * Created date:下午2:25:46
 */
package com.cmcc.zysoft.train.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;

import com.cmcc.zysoft.train.common.BaseController;
import com.cmcc.zysoft.train.model.UserVO;
import com.cmcc.zysoft.train.service.UserService;

/**
 * @author lisanlai
 * @mail sanlai_lee@qq.com
 * @date 2013-6-5 下午2:25:46
 */
@RequestMapping("/")
public class UserController extends BaseController {

	/**
	 * 用户业务逻辑层
	 */
	@Resource
	private UserService userService;
	
	/**
	 * 用户登录
	 * @param request
	 * @return  返回HashMap <br />
	 * 返回数据实例：<br />
	 * <p>
	 * <pre>
	 * 登录成功:<br />{
	 * 	succeess:true
	 * }
	 * 
	 * 登录失败:<br />{
	 * 	succeess:true
	 * }
	 * </pre>
	 * </p>
	 */
	public Map<String, Object> login(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String, Object>();
		String username =  request.getParameter("username");
		String password =  request.getParameter("password");
		UserVO userVO = userService.login(username, password);
		if(userVO!=null){
			map.put("success", true);
		}else{
			map.put("success", false);
		}
		return map;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
}
