package com.cmcc.zysoft.train.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.cmcc.zysoft.train.model.UserVO;
import com.cmcc.zysoft.train.service.UserService;

/**
 * 测试UserController
 * @author lisanlai
 * @mail sanlai_lee@qq.com
 * @date 2013-6-5 下午3:03:11
 */
public class UserControllerTest {
	
  /**
   * 测试UserController的login方法
   */
  @Test
  public void login() {
	  String username = "lisanlai";
	  String password = "111111";
	  
	  //创建mock对象创建的控制器
	  IMocksControl control = EasyMock.createControl();
	  //创建第一个HttpServletRequest虚拟对象
	  HttpServletRequest request = control.createMock(HttpServletRequest.class);
	  //创建第二个HttpServletRequest虚拟对象
	  HttpServletRequest request1 = control.createMock(HttpServletRequest.class);
	  //创建第一个UserService虚拟对象
	  UserService userService = control.createMock(UserService.class);
	  //创建第二个UserService虚拟对象
	  UserService userService1 = control.createMock(UserService.class);
	  
	  //创建一个UserVO的虚拟对象
	  UserVO userVO = control.createMock(UserVO.class);
	  //设置虚拟VO对象的一些属性值
	  userVO.setUsername(username);
	  userVO.setPassword(password);
	  
	  //录制第一个HttpServletRequest虚拟对象要完成的动作，并设计此动作要返回的数据
	  EasyMock.expect(request.getParameter("username")).andReturn("lisanlai");
	  EasyMock.expect(request.getParameter("password")).andReturn("111111");
	  //录制第二个HttpServletRequest虚拟对象要完成的动作，并设计此动作要返回的数据
	  EasyMock.expect(request1.getParameter("username")).andReturn("lisanlai");
	  EasyMock.expect(request1.getParameter("password")).andReturn("222222");
	  
	  //录制第一个UserService虚拟对象要完成的动作，并设计此动作要返回的数据
	  EasyMock.expect(userService.login("lisanlai", "111111")).andReturn(userVO);
	  //录制第二个UserService虚拟对象要完成的动作，并设计此动作要返回的数据
	  EasyMock.expect(userService1.login("lisanlai", "222222")).andReturn(null);
	  
	  //设置control的状态为replay状态
	  control.replay();
	  
	  //创建一个要测试的对象
	  UserController userController = new UserController();
	  //利用依赖注入的方式设置userService属性，这个地方如果结合spring测试，那么可以不用set方式进行注入
	  userController.setUserService(userService);
	  
	  //执行要测试的方法，并获得返回值
	  Map<String, Object> map = userController.login(request);
	  
	  //将返回的值与预期的值进行对比，来测试方法运行是否与预期相同
	  Assert.assertEquals(true, (boolean)map.get("success"));
	  
	  //再测一次设计好的错误的逻辑，是否也符合预期
	  userController.setUserService(userService1);
	  map = userController.login(request1);
	  Assert.assertEquals(false, (boolean)map.get("success"));
	  
  }
}
