package com.cmcc.zysoft.train.dao;

import com.cmcc.zysoft.train.model.UserVO;
import com.cmcc.zysoft.train.model.UserVOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserVOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    int countByExample(UserVOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    int deleteByExample(UserVOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    int insert(UserVO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    int insertSelective(UserVO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    List<UserVO> selectByExample(UserVOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    UserVO selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    int updateByExampleSelective(@Param("record") UserVO record, @Param("example") UserVOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    int updateByExample(@Param("record") UserVO record, @Param("example") UserVOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    int updateByPrimaryKeySelective(UserVO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_user
     *
     * @mbggenerated Wed Jun 05 13:23:00 CST 2013
     */
    int updateByPrimaryKey(UserVO record);
}